# CellGPU 

CellGPU implements GPU-accelerated algorithms to simulate off-lattice models of cells. Its current
two main feature sets focus on a Voronoi-decomposition-based model of two-dimensional monolayers (the "self-propelled Voronoi model")
and on a two-dimensional dynamical version of the vertex model.

The main cellGPU repository has moved to
https://gitlab.com/dmsussman/cellGPU
Please see that page for access to the up-to-date versions of code.

Documentation is additionally being maintained at
http://dmsussman.gitlab.io/cellGPUdocumentation/

A paper describing the general outline of the program is available at https://arxiv.org/abs/1702.02939